//[OBJECTIVE] Create a server-side app using Express Web Framework. 

// [SECTION] Create an application using express
const express = require("express");

// Identify a designated location or address where the connection will be created or served.
const port = 4000;

// our objective here is to establish a connection.
// express() => this creates an express application.
// In Layman's terms, application is our server.

const application = express();

// Assign/Bind the connection to the designated desired address or port.
// listen() => this allows us to create a listener on the specified port or location.
// SYNTAX: serverName.listen(port, callback)

application.listen(port, () => console.log(`Express API Server on port: ${port}`));

// Send a screenshot of your current output running 